require 'rails_helper'

RSpec.describe Subscriber, type: :model do
  it { should have_many(:subscriptions) }
  it { should have_many(:rules) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email) }
end
