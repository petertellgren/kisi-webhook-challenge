require 'rails_helper'

RSpec.describe Rule, type: :model do
  it { should have_many(:triggers) }
  it { should have_many(:subscriptions) }
  it { should have_many(:subscribers) }

  it { should validate_presence_of(:name) }

  let(:run_date) { Date.today+1.day }
  let(:run_time) { "22:00:15" }
  let!(:active_rule) do
    create(
      :rule,
      start_date: run_date-1.day,
      end_date: run_date+1.day,
      start_time: "18:00:00",
      stop_time: "06:30:00"
    )
  end
  let!(:inactive_rule) do
    create(
      :rule,
      start_date: run_date+2.day,
      end_date: run_date+3.days,
      start_time: "10:00:00",
      stop_time: "17:30:00"
    )
  end

  describe ".active" do
    context 'given a valid datetime argument' do
      let(:datetime) do
        run_date.to_datetime + Time.parse(run_time).seconds_since_midnight.seconds
      end
      subject { Rule.active(datetime) }

      it "returns only active rules for given date and time window" do
        expect(subject.count).to eq(1)
        expect(subject.first).to eq(active_rule)
      end

      it "reutrns as a ar collection to allow for chaining" do
        expect(subject).to be_a(ActiveRecord::Relation)
      end
    end

    context 'given invalid datetime' do
      subject { Rule.active(nil) }

      it 'returns an empty collection' do
        expect(subject.count).to eq(0)
        expect(subject).to be_a(ActiveRecord::Relation)
      end
    end
  end

  describe ".matching" do
    let(:payload) { Hash.new }
    subject { Rule.matching(payload) }

    it "returns all rules with matching triggers" do
      expect(subject.count).to eq(2)
    end

    it 'returns as AR collection to allow for cahinging' do
      expect(subject).to be_a(ActiveRecord::Relation)
    end
  end

  describe "#match?" do
    let!(:trigger) { create(:trigger, rule_id: active_rule.id) }
    let(:payload) { { key: 'value' } }
    let(:datetime) do
      run_date.to_datetime + Time.parse(run_time).seconds_since_midnight.seconds
    end

    it "returns true when all triggers match" do
      expect_any_instance_of(Trigger).to receive(:match?).with(payload).and_return(true)
      expect(active_rule.match?(payload)).to be_truthy
    end

    it "returns true when any triggers don't match" do
      expect_any_instance_of(Trigger).to receive(:match?).with(payload).and_return(false)
      expect(active_rule.match?(payload)).to be_falsey
    end
  end

  describe "#in_date_range?" do
    it "returns true for an active rules" do
      rules = Rule.all.select { |r| r.in_date_range?(run_date) }
      expect(rules.length).to eq(1)
      expect(rules.first).to eq(active_rule)
    end
  end

  describe "#in_time_range?" do
    it "returns true for an active rule" do
      rules = Rule.all.select { |r| r.in_time_range?(run_time) }
      expect(rules.length).to eq(1)
      expect(rules.first).to eq(active_rule)
    end
  end
end
