require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:event) { create(:event) }
  describe "created_at" do
    it "gets timestamp from iniside the json payload" do
      expect(event.created_at).to eq(event.payload['created_at'])
    end
  end
end
