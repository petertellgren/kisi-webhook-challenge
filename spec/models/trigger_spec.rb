require 'rails_helper'

RSpec.describe Trigger, type: :model do
  it { should belong_to(:rule) }
  it { should validate_presence_of(:key) }

  describe '#match?' do
    let(:event) {
      create(:event, payload: { action: 'unlock', success: 'true' } )
    }
    let(:payload) { event.payload }

    describe 'trigger with no values' do
      it "returns true when key is matched to key in payload" do
        trigger = build(:trigger, key: 'action', values: '')
        expect(trigger.match?(payload)).to be_truthy
      end

      it 'returns false when key is not found in payload' do
        trigger = build(:trigger, key: 'no_key', values: '')
        expect(trigger.match?(payload)).to be_falsy
      end
    end

    describe 'trigger with one value' do
      it 'returns true when key and value is matched to key in payload' do
        trigger = build(:trigger, key: 'action', values: ['unlock'])
        expect(trigger.match?(payload)).to be_truthy
      end

      it 'returns false when value does not match value in payload' do
        trigger = build(:trigger, key: 'action', values: ['lock'])
        expect(trigger.match?(payload)).to be_falsy
      end
    end

    describe 'trigger with mutliple values' do
      it 'returns true when key and any values matched to key and value in payload' do
        trigger = build(:trigger, key: 'action', values: ['unlock', 'lock'])
        expect(trigger.match?(payload)).to be_truthy
      end

      it 'returns false when no values match value in payload' do
        trigger = build(:trigger, key: 'action', values: ['open', 'close'])
        expect(trigger.match?(payload)).to be_falsy
      end
    end

  end
end
