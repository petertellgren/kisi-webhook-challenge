require 'rails_helper'

RSpec.describe 'Subscriptions API', type: :request do
  let(:rule) { create(:rule) }
  let(:rule_id) { rule.id }
  let(:subscribers) { create_list(:subscriber, 4) }
  let!(:subscription) do
    Subscription.create(
      rule_id: rule_id, subscriber_id: subscribers.first.id
    )
  end

  describe 'POST /rules/:rule_id/subscriptions' do
    let(:valid_params) { { emails: subscribers.map(&:email) } }

    context 'when request params are valid' do
      before { post "/rules/#{rule_id}/subscriptions", params: valid_params }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'returns status code 201' do
        expect(json.last['subscriber_id']).to eq(subscribers.last.id)
      end
    end
  end

  describe 'DELETE /rules/:rule_id/subscriptions/:id' do
    before { delete "/rules/#{rule_id}/subscriptions/#{subscription.id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end
