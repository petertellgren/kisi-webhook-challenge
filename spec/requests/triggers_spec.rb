require 'rails_helper'

RSpec.describe 'Triggers API', type: :request do
  let(:rule) { create(:rule) }
  let!(:triggers) { create_list(:trigger, 20, rule_id: rule.id) }
  let(:rule_id) { rule.id }
  let(:id) { triggers.first.id }

  describe 'GET /rules/:rule_id/triggers' do
    before { get "/rules/#{rule_id}/triggers" }

    context 'when rule exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all rule triggers' do
        expect(json.size).to eq(20)
      end
    end

    context 'when rule does not exist' do
      let(:rule_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Rule/)
      end
    end
  end

  describe 'GET /rules/:rule_id/triggers/:id' do
    before { get "/rules/#{rule_id}/triggers/#{id}" }

    context 'when rule trigger exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the trigger' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when rule trigger does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Trigger/)
      end
    end
  end

  describe 'POST /rules/:rule_id/triggers' do
    let(:valid_params) { { key: 'action', values: ['unlock', 'lock'] } }

    context 'when request params are valid' do
      before { post "/rules/#{rule_id}/triggers", params: valid_params }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'returns the trigger' do
        expect(json['key']).to eq(valid_params[:key])
        expect(json['values']).to eq(valid_params[:values])
      end
    end

    context 'when an invalid request' do
      before { post "/rules/#{rule_id}/triggers", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Key can't be blank/)
      end
    end
  end

  describe 'PUT /rules/:rule_id/triggers/:id' do
    let(:valid_params) { { key: 'Status' } }

    before { put "/rules/#{rule_id}/triggers/#{id}", params: valid_params }

    context 'when trigger exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the trigger' do
        updated_trigger = Trigger.find(id)
        expect(updated_trigger.key).to match(/Status/)
      end
    end

    context 'when the trigger does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Trigger/)
      end
    end
  end

  describe 'DELETE /rules/:rule_id/triggers/:id' do
    before { delete "/rules/#{rule_id}/triggers/#{id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
