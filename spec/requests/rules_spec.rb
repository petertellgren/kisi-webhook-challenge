require 'rails_helper'

RSpec.describe 'Rules API', type: :request do
  # initialize test data
  let!(:rules) { create_list(:rule, 10) }
  let(:rule_id) { rules.first.id }

  describe 'GET /rules' do
    before { get '/rules' }

    it 'returns rules' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /rules/:id' do
    before { get "/rules/#{rule_id}" }

    context 'when the record exists' do
      it 'returns the rule' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(rule_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:rule_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Rule/)
      end
    end
  end

  describe 'POST /rules' do
    # valid payload
    let(:valid_params) do
      {
        name: 'Crazy Rule'
      }
    end

    context 'when the request is valid' do
      before { post '/rules', params: valid_params }

      it 'creates a rule' do
        expect(json['name']).to eq('Crazy Rule')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/rules', params: { name: '' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Name can't be blank/)
      end
    end
  end

  describe 'PUT /rules/:id' do
    let(:valid_attributes) { { Name: 'Smart Rule', end_date: '2099-10-29' } }

    context 'when the record exists' do
      before { put "/rules/#{rule_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /rules/:id' do
    before { delete "/rules/#{rule_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
