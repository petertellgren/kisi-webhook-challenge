require 'rails_helper'

RSpec.describe 'Events API', type: :request do
  let(:webhook) do
    {
      actor_type: 'User',
      actor_id: 1,
      actor_email: 'a@b.com',
      action: 'unlock',
      object_type: 'Lock',
      object_id: 1,
      success: 'true',
      code: 'ffffff',
      message: 'a@b.com unlocked lock Entrance Door',
      references: [
        {
          type: 'Place',
          id: 1
        },
        {
          type: 'Lock',
          id: 1
        }
      ],
      created_at: '2017-12-24T19:20:00.511Z'
    }
  end

  let!(:rule_ok) { create(:rule) }
  let!(:trigger) do
    create(
      :trigger, key: 'action', values: ['unlock', 'lock'], rule_id: rule_ok.id
    )
  end
  let!(:rule_nope) { create(:rule) }
  let!(:trigger) do
    create(
      :trigger, key: 'action', values: ['lock'], rule_id: rule_nope.id
    )
  end
  let!(:subscribers) { create_list(:subscriber, 3) }

  before do
    subscribers[0..1].each do |s|
      Subscription.create!(rule: rule_ok, subscriber: s)
    end
    subscribers[1..2].each do |s|
      Subscription.create!(rule: rule_nope, subscriber: s)
    end
  end

  describe 'POST /events' do
    context 'when the event triggers an notification' do
      before { post "/events", params: webhook }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'has created an event with relevant payload body' do
        event = Event.last
        expect(event.payload['action']).to eq(webhook[:action])
      end
    end

    context 'when the event has no date value' do
      before { post "/events", params: webhook.delete(:created_at) }

      it 'returns status error and code 422' do
        expect(response).to have_http_status(422)
      end
    end

    context 'when there are no matching rules for the event' do
      it 'returns 404' do
        expect(Rule).to receive(:active).and_return(Rule.where(id: []))
         post "/events", params: webhook
        expect(response).to have_http_status(404)
      end
    end
  end
end
