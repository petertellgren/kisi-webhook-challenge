require 'rails_helper'

RSpec.describe RulesController, type: :routing do
  describe 'admin rules routing' do
    it 'routes GET /rules to rules#index' do
      expect(:get => '/rules').to route_to('rules#index')
    end
  end
  describe 'admin triggers routing' do
    it 'routes GET /rules/:rule_id/triggers to triggers#index' do
      expect(:get => '/rules/1/triggers').to route_to(
        :controller => 'triggers',
        :action => 'index',
        :rule_id => '1'
      )
    end
  end
end

RSpec.describe SubscribersController, type: :routing do
  describe 'admin subscriber routing' do
    it 'routes GET /subscribers to subscribers#index' do
      expect(:get => '/subscribers').to route_to('subscribers#index')
    end
  end
end
