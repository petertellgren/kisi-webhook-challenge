require "rails_helper"

RSpec.describe NotificationMailer, type: :mailer do
  describe "event_mail" do
    let(:event) { create(:event) }
    let(:rule_names) { ['rule_a', 'rule_b'] }
    let(:email) { "someone@example.com" }

    let(:mail) { NotificationMailer.event(email, rule_names, event) }

    it "sends an email to the email address in notificaion" do
      expect(mail.to).to eq([email])
    end

    it "sends from the service email address" do
      expect(mail.from).to eq(['notifications@example.com'])
    end

    it "sends with the correct subject" do
      expect(mail.subject).to eq('Kisi Event')
    end

    it "generates a multipart message (plain text and html)" do
      expect(mail.body.parts.length).to eq(2)
      expect(mail.body.parts.collect(&:content_type)).to eq(["text/plain; charset=UTF-8", "text/html; charset=UTF-8"])
    end

    context "html version" do
      let(:content) { get_message_part(mail, /html/) }
      it "contains the actor email" do
        expect(content).to match(/rule_a/)
      end
    end

    context "text version" do
      let(:content) { get_message_part(mail, /plain/) }
      it "contains the actor email" do
        expect(content).to match(/rule_a/)
      end
    end
  end
end
