require 'rails_helper'

RSpec.describe SubscriptionList, type: :model do

  describe ".build" do
    let(:rule) { create(:rule) }
    let(:subscribers) { create_list(:subscriber, 5) }
    let(:emails) do
      #subset of subscribers, + not persisted
      subscribers[0..2].map(&:email) + [build(:subscriber).email]
    end

    before do
      subscribers[3..-1].each do |s|
        Subscription.create(rule_id: rule.id, subscriber_id: s.id)
      end
    end

    it "returns a hash with only the new subscriptions" do
      list = SubscriptionList.build(rule, emails)

      expect(list).to eq(
        [
          { rule_id: rule.id, subscriber_id: subscribers[0].id },
          { rule_id: rule.id, subscriber_id: subscribers[1].id },
          { rule_id: rule.id, subscriber_id: subscribers[2].id }
        ]
      )
    end
  end
end
