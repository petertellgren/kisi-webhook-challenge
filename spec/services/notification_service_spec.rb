require 'rails_helper'

RSpec.describe NotificationService, type: :model do

  describe '.notify' do
    let(:rule) { create(:rule) }
    let(:subscriber) { create(:subscriber) }
    let(:event) { create(:event) }

    it "builds a list and sends out the emails" do

      expect(NotificationService).to receive(:build_recipients_list).
        with([rule]).and_return(
          { subscriber.email => [rule.id] }
        )

      NotificationService.notify([rule], event)

      mail = ActionMailer::Base.deliveries.last

      aggregate_failures do
        expect(mail.to).to eq [subscriber.email]
        expect(mail.from).to eq ["notifications@example.com"]
        expect(mail.subject).to eq "Kisi Event"
        html_content = get_message_part(mail, /html/)
        expect(html_content).to match rule.name
      end
    end
  end

  describe '.build_recipients_list' do
    let(:rules) { create_list(:rule, 3) }
    let(:subscribers) { create_list(:subscriber, 3) }
    before do
      subscribers.each do |sub|
        case sub.id
        when subscribers[0].id
          rules[0..1].each do |r|
            Subscription.create!(rule: r, subscriber: sub)
          end
        when subscribers[1].id
          Subscription.create!(rule: rules[1], subscriber: sub)
        when subscribers[2].id
          rules[1..-1].each do |r|
            Subscription.create!(rule: r, subscriber: sub)
          end
        end
      end
    end

    it 'returns a list of recipients linked to the matched rules' do
      expected_list = {
        subscribers.first.email => rules[0..1].map(&:id),
        subscribers[1].email => [rules[1].id],
        subscribers.last.email => rules[1..-1].map(&:id)
      }
      recipients = NotificationService.build_recipients_list(rules)
      expect(recipients).to eq(expected_list)
    end
  end
end
