FactoryBot.define do
  factory :event do
    payload do
      {
        actor_type: 'User',
        actor_id: 1,
        actor_email: 'a@b.com',
        action: 'unlock',
        object_type: 'Lock',
        object_id: 1,
        success: 'true',
        code: 'ffffff',
        message: 'a@b.io unlocked lock Entrance Door',
        created_at: '2017-12-24T19:20:00.511Z'
      }
    end
  end
end
