FactoryBot.define do
  factory :rule do
    sequence :name do |n|
      "rule_#{n}"
    end
    start_date "2017-11-10"
    end_date "2018-01-01"
  end
end
