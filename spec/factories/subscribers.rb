FactoryBot.define do
  factory :subscriber do
    sequence :email do |n|
      "email_#{n}@example.com"
    end
  end
end
