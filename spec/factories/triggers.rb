FactoryBot.define do
  factory :trigger do
    sequence :key do |n|
      "trigger_#{n}"
    end
    values ['yes', 'no']
    rule_id nil
  end
end
