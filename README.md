# KISI Webhook challenge

The challenge was to build a web service that given a [Webhook](http://docs.kisiapi.apiary.io/#introduction/integrations/webhooks). 

Runs the payload through a set of rules to determine if an email should be sent to the subscribers.

### Solution

My solution is built on Rails 5 api backed by Postgresql and SendGrid in order to save time.
The code is tested with RSpec.

The application has the following models

**Rule:**   
A Rule has a start and End Date, as well as a start/stop time.. this so that the rule can be active for a given period of time for many days. 

improvements in production would be bitmask allowing it to be active for certain weekdays during the date period.

**Trigger:**  
A rule has one or several triggers, each trigger has a keyword and values.
example
```
  key: 'action'
  values: ['unlock', 'lock']
```

for a rule to be active all child triggers need to resolve truthy when passed the webhook payload.

**Subscriber:**  
simply a representation of a potential recipient

**Subscription:**  
ties a subscriber to a given Rule.

**Event:**  
persists the incoming webhook payload in order for future background processing.


##Application flow:

###Configuration

```
# Crete Rule
curl -X "POST" "https://URL/rules" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "name": "Demo Rule",
  "start_date": "2017-10-27",
  "end_date": "2027-11-27",
  "start_time": "00:30:00",
  "stop_time": "23:30:00"
}'

## Create Trigger
curl -X "POST" "https://URL/rules/:id/triggers" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "key": "action",
  "values": [
    "unlock",
    "lock"
  ]
}'

## Create Subscriber
curl -X "POST" "https://URL/subscribers" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "email": "email@example.com"
}'

## Create Subscription
curl -X "POST" "https://URL/rules/:id/subscriptions" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "emails": [
    "email@example.com"
  ]
}'
```

### Operation

when the configuration is done the instance is ready to recieve webhooks..

example:
```
curl -X "POST" "https://URL/events" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "success": "true",
  "actor_type": "User",
  "created_at": "2017-12-24T19:20:00.511Z",
  "object_type": "Lock",
  "action": "unlock"
}'
```




