Rails.application.routes.draw do
  resources :events, only: [:create]
  resources :rules do
    resources :triggers
    resources :subscriptions, only: [:create, :destroy]
  end
  resources :subscribers
end
