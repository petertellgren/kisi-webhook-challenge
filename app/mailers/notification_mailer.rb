class NotificationMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def event(email, rule_names, event)
    # do somethign with rule names and event
    @rule_names = rule_names
    @event_payload = JSON.pretty_generate(event.payload)
    mail(to: email, subject: 'Kisi Event')
  end
end
