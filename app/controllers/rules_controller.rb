class RulesController < ApplicationController
  before_action :set_rule, only: [:show, :update, :destroy]

  def index
    @rules = Rule.all
    json_response(@rules)
  end

  def create
    @rule = Rule.create!(rule_params)
    json_response(@rule, :created)
  end

  def show
    json_response(@rule)
  end

  def update
    @rule.update!(rule_params)
    head :no_content
  end

  def destroy
    @rule.destroy
    head :no_content
  end

  private

  def rule_params
    params.permit(:name, :start_date, :end_date, :start_time, :stop_time)
  end

  def set_rule
    @rule = Rule.find(params[:id])
  end
end
