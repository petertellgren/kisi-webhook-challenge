class EventsController < ApplicationController

  def create
    event_time = DateTime.parse(event_params.fetch(:created_at, ''))
    event = Event.create!(payload: event_params)
    matched = Rule.active(event_time).matching(event.payload)
    raise ActiveRecord::RecordNotFound, 'No matching rules' if matched.empty?
    NotificationService.notify(matched, event)
    head :created
  end

  private

  def event_params
    if request.method == "POST"
      params[:action] = request.request_parameters['action']
    else
      params[:action] = request.query_parameters['action']
    end
    params.permit(:created_at, :actor_email, :action, :object_type, :success)
  end
end
