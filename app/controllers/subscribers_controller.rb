class SubscribersController < ApplicationController
  before_action :set_subscriber, only: [:show, :update, :destroy]

  def index
    @subscribers = Subscriber.all
    json_response(@subscribers)
  end

  def create
    @subscriber = Subscriber.create!(subscriber_params)
    json_response(@subscriber, :created)
  end

  def show
    json_response(@subscriber)
  end

  def update
    @subscriber.update!(subscriber_params)
    head :no_content
  end

  def destroy
    @subscriber.destroy
    head :no_content
  end

  private

  def subscriber_params
    params.permit(:email)
  end

  def set_subscriber
    @subscriber = Subscriber.find(params[:id])
  end
end
