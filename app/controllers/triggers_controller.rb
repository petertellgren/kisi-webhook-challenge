class TriggersController < ApplicationController
  before_action :set_rule
  before_action :set_rule_trigger, only: [:show, :update, :destroy]

  def index
    json_response(@rule.triggers)
  end

  def show
    json_response(@trigger)
  end

  def create
    @trigger = @rule.triggers.create!(trigger_params)
    json_response(@trigger, :created)
  end

  def update
    @trigger.update(trigger_params)
    head :no_content
  end

  def destroy
    @trigger.destroy
    head :no_content
  end

  private

  def trigger_params
    params.permit(:key, values: [])
  end

  def set_rule
    @rule = Rule.find(params[:rule_id])
  end

  def set_rule_trigger
    @trigger = @rule.triggers.find_by!(id: params[:id]) if @rule
  end
end
