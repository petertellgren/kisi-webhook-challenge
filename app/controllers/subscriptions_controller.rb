class SubscriptionsController < ApplicationController
  before_action :set_rule

  def create
    subscriptions = SubscriptionList.build(@rule, subscription_params[:emails])
    Subscription.create!(subscriptions)
    json_response(@rule.subscriptions, :created)
  end

  def destroy
    subscription = @rule.subscriptions.find_by!(id: params[:id])
    subscription.destroy
    head :no_content
  end

  private

  def subscription_params
    params.permit(emails: [])
  end

  def set_rule
    @rule = Rule.find(params[:rule_id])
  end

end

