class Rule < ApplicationRecord
  before_create :set_dates_if_nil

  has_many :triggers
  has_many :subscriptions
  has_many :subscribers, through: :subscriptions

  validates :name, presence: true
  validate :start_date_cannot_be_in_the_past
  validate :end_date_cannot_be_before_start_date
  validates :start_time, :stop_time, format: { with: /\A[0-2]\d\:[0-5]\d\:[0-5]\d\z/,
    message: "only allows 24h times with format HH:MM:SS" }

  def self.active(datetime)
    rules = all.select { |rule| rule.active?(datetime) }
    where(id: rules.map(&:id))
  end

  def self.matching(payload)
    rules = all.select { |rule| rule.match?(payload) }
    where(id: rules.map(&:id))
  end

  def active?(datetime)
    return false if datetime.nil? || !datetime.kind_of?(DateTime)
    date = datetime.to_date
    time = datetime.strftime("%H:%M:%S")
    in_date_range?(date) && in_time_range?(time)
  end

  def match?(payload)
    triggers.map{ |trigger| trigger.match?(payload) }.all?{ |r| r == true }
  end

  def in_date_range?(date)
    date >= start_date && date <= end_date
  end

  def in_time_range?(time)
    start = time_to_s(start_time)
    stop = time_to_s(stop_time)
    range = start < stop ? start..stop : [*0..stop, *start..86400]
    range.include?(time_to_s(time))
  end

  private

  def set_dates_if_nil
    self.start_date = Date.today if self.start_date.nil?
    self.end_date = Date.today + 10.years if self.end_date.nil?
  end

  def time_to_s(time)
    Time.parse(time).seconds_since_midnight.seconds.to_i
  end

  def start_date_cannot_be_in_the_past
    if start_date.present? && start_date < Date.today
      errors.add(:start_date, "can't be in the past")
    end
  end

  def end_date_cannot_be_before_start_date
    if end_date.present? && end_date < start_date
      errors.add(:end_date, "can't be before start date")
    end
  end

end
