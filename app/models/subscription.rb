class Subscription < ApplicationRecord
  belongs_to(:rule)
  belongs_to(:subscriber)
end
