class Trigger < ApplicationRecord
  belongs_to :rule

  validates :key, presence: true

  def match?(payload)
    match = false
    if payload.keys.include?(key)
      match = values.empty? || values.include?(payload[key])
    end
  end
end
