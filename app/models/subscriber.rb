class Subscriber < ApplicationRecord
  has_many :subscriptions
  has_many :rules, through: :subscriptions

  validates :email, presence: true, uniqueness: true
end
