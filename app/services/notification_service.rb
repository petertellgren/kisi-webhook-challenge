module NotificationService
  module_function

  def notify(rules, event)
    recipients = self.build_recipients_list(rules)
    recipients.each do |recipient, ids|
      rule_names = rules.select { |r| ids.include?(r.id) }.map(&:name)
      NotificationMailer.event(recipient, rule_names, event).deliver
    end
  end

  def build_recipients_list(rules)
    recipients = Hash.new []
    rules.each do |rule|
      rule.subscribers.map(&:email).each do |email|
        recipients[email] += [rule.id]
      end
    end
    recipients
  end
end
