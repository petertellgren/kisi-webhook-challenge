module SubscriptionList
  module_function

  def build(rule, emails)
    emails = emails.nil? ? [] : emails
    existing = rule.subscribers.map(&:email)
    emails = emails - existing
    subscribers = Subscriber.where(email: emails)
    subscribers.map do |s|
      {
        rule_id: rule.id,
        subscriber_id: s.id
      }
    end
  end
end
