class CreateRules < ActiveRecord::Migration[5.1]
  def change
    create_table :rules do |t|
      t.string :name, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.string :start_time, default: "12:00:01"
      t.string :stop_time, default: "12:00:00"

      t.timestamps
    end
  end
end
