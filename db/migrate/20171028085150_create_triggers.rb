class CreateTriggers < ActiveRecord::Migration[5.1]
  def change
    create_table :triggers do |t|
      t.references :rule, null: false, foreign_key: true
      t.string :key, null: false
      t.text :values, array: true, default: []

      t.timestamps
    end
  end
end
