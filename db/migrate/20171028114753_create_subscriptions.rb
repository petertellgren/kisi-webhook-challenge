class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.belongs_to :rule, index: true
      t.belongs_to :subscriber, index: true

      t.timestamps
    end
  end
end
